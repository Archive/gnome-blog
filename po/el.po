# translation of gnome-blog.po to Greek
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Nikolaos Agianniotis <nickagian@gmail.com>, 2007.
# Athanasios Lefteris <alefteris@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: gnome-blog 0.9.2\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-blog&component=general\n"
"POT-Creation-Date: 2010-03-04 21:10+0000\n"
"PO-Revision-Date: 2012-07-26 12:11+0200\n"
"Last-Translator: Tom Tryfonidis <tomtryf@gmail.com>\n"
"Language-Team: Greek <team@gnome.gr>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../GNOME_BlogApplet.server.in.in.h:1
msgid "Accessories"
msgstr "Βοηθήματα"

#: ../GNOME_BlogApplet.server.in.in.h:2
#: ../gnome-blog.desktop.in.in.h:1
msgid "Blog Entry Poster"
msgstr "Δημοσίευση καταχωρήσεων ιστολογίου"

#: ../GNOME_BlogApplet.server.in.in.h:3
msgid "Post entries to a web log"
msgstr "Δημοσίευση καταχωρήσεων σε ένα ιστολόγιο"

#: ../GNOME_BlogApplet.xml.h:1
msgid "_About..."
msgstr "_Περί..."

#: ../GNOME_BlogApplet.xml.h:2
msgid "_Preferences..."
msgstr "_Προτιμήσεις..."

#: ../blog_applet.py:34
msgid "About"
msgstr "Περί"

#: ../blog_applet.py:37
msgid "Blog"
msgstr "Ιστολόγιο"

#: ../blog_applet.py:89
msgid "A GNOME Web Blogging Applet"
msgstr "Ένα GNOME εφαρμογίδιο για δημοσίευση σε ιστολόγια"

#: ../blog_applet.py:91
msgid "translator-credits"
msgstr ""
"Ελληνική μεταφραστική ομάδα GNOME\n"
" Νκόλαος Αγιαννιώτης <nickagian@gmail.com>\n"
"\n"
"Για περισσότερα δείτε http://www.gnome.gr/"

#: ../blog_applet.py:128
#, python-format
msgid "Create a blog entry for %s at %s"
msgstr "Δημιουργία καταχώρησης ιστολογίου για %s στο %s"

#: ../blog_poster.py:44
msgid "_Post Entry"
msgstr "_Δημοσίευση καταχώρησης"

#: ../blog_poster.py:69
msgid "Preferences..."
msgstr "Προτιμήσεις..."

#: ../blog_poster.py:81
msgid "Title:"
msgstr "Τίτλος:"

#: ../blog_poster.py:88
msgid "Tags:"
msgstr "Ετικέτες:"

#: ../blog_poster.py:109
#| msgid "_Post Entry"
msgid "Post Blog Entry"
msgstr "Δημοσίευση καταχώρησης ιστολογίου"

#: ../blog_poster.py:133
msgid "Couldn't upload images"
msgstr "Δεν ήταν δυνατή η αποστολή εικόνων"

#: ../blog_poster.py:133
msgid "The blog protocol in use does not support uploading images"
msgstr "Το πρωτόκολλο ιστολογίου που χρησιμοποιείται δεν υποστηρίζει την αποστολή εικόνων"

#: ../blog_poster.py:183
msgid "Blog Entry is Blank"
msgstr "Η καταχώρηση ιστολογίου είναι κενή"

#: ../blog_poster.py:183
msgid "No text was entered in the blog entry box. Please enter some text and try again"
msgstr "Δεν εισήχθη κείμενο στο πλαίσιο καταχώρησης ιστολογίου. Παρακαλώ εισάγετε κάποιο κείμενο και προσπαθήστε πάλι"

#: ../blogger_prefs.py:19
msgid "Blogger Preferences"
msgstr "Προτιμήσεις συγγραφέα ιστολογίου"

#: ../blogger_prefs.py:39
msgid "Self-Run MovableType"
msgstr "MovableType διαχειριζόμενο από εσάς"

#: ../blogger_prefs.py:40
msgid "Self-Run Pyblosxom"
msgstr "Pyblosxom διαχειριζόμενο από εσάς"

#: ../blogger_prefs.py:41
msgid "Self-Run WordPress"
msgstr "WordPress διαχειριζόμενο από εσάς"

#: ../blogger_prefs.py:42
msgid "Self-Run Other"
msgstr "Άλλου τύπου ιστολόγιο διαχειριζόμενο από εσάς"

#: ../blogger_prefs.py:46
msgid "Blog Type:"
msgstr "Τύπος ιστολογίου:"

#: ../blogger_prefs.py:56
msgid "Blog Protocol:"
msgstr "Πρωτόκολλο ιστολογίου:"

#: ../blogger_prefs.py:60
#: ../blogger_prefs.py:122
#: ../blogger_prefs.py:127
#: ../blogger_prefs.py:132
msgid "Base Blog URL:"
msgstr "URL βάσης ιστολογίου:"

#: ../blogger_prefs.py:62
msgid "Blog Name:"
msgstr "Όνομα ιστολογίου:"

#: ../blogger_prefs.py:64
msgid "Lookup Blogs"
msgstr "Αναζήτηση ιστολογίων"

#: ../blogger_prefs.py:74
msgid "Username:"
msgstr "Όνομα χρήστη:"

#: ../blogger_prefs.py:75
msgid "Password:"
msgstr "Κωδικός πρόσβασης:"

#: ../blogger_prefs.py:117
#: ../blogger_prefs.py:137
#: ../blogger_prefs.py:147
#: ../blogger_prefs.py:153
msgid "XML-RPC URL:"
msgstr "XML-RPC URL:"

#: ../blogger_prefs.py:142
msgid "ATOM-based"
msgstr "Βασισμένο στο ATOM"

#: ../blogger_prefs.py:161
msgid "Unknown blog type"
msgstr "Άγνωστος τύπος ιστολογίου"

#: ../blogger_prefs.py:161
msgid "The detected blog type is not among the list of supported blogs"
msgstr "Ο τύπος ιστολογίου που ανιχνεύτηκε δε βρίσκεται στη λίστα με τα υποστηριζόμενα ιστολόγια"

#: ../gnome-blog.desktop.in.in.h:2
msgid "Post an entry to a web log"
msgstr "Δημοσίευση καταχώρησης στο ιστολόγιο"

#: ../gnomeblog.schemas.in.h:1
msgid "Password to use in accessing the blog"
msgstr "Ο κωδικός πρόσβασης προς χρήση για την προσπέλαση του ιστολογίου"

#: ../gnomeblog.schemas.in.h:2
msgid "Protocol to use in accessing the blog"
msgstr "Το πρωτόκολλο προς χρήση για την προσπέλαση του ιστολογίου"

#: ../gnomeblog.schemas.in.h:3
msgid "Suffix to attach to the base URL (e.g. \"xmlrpc.cgi\")"
msgstr "Κατάληξη για επισύναψη στο URL βάσης (π.χ. \"xmlrpc.cgi\")"

#: ../gnomeblog.schemas.in.h:4
msgid "The application's preferences have been initialized"
msgstr "Οι προτιμήσεις της εφαρμογής έχουν αρχικοποιηθεί"

#: ../gnomeblog.schemas.in.h:5
msgid "The id or name of the particular blog to use on the XML-RPC server"
msgstr "Το αναγνωριστικό ή το όνομα του συγκεκριμένου ιστολογίου προς χρήση στον XML-RPC εξυπηρετητή"

#: ../gnomeblog.schemas.in.h:6
msgid "URL to the bloggerAPI compatible XML-RPC server"
msgstr "Το URL προς τον XML-RPC εξυπηρετητή που είναι συμβατός με το bloggerAPI"

#: ../gnomeblog.schemas.in.h:7
msgid "Username to use in accessing the blog"
msgstr "Το όνομα χρήστη προς χρήση για πρόσβαση στο ιστολόγιο"

#: ../hig_alert.py:15
#, python-format
msgid "URL '%s' may not be a valid bloggerAPI. XML-RPC Server reported: %s."
msgstr "Το URL '%s' πιθανώς να μην είναι κάποιο έγκυρο bloggerAPI. Ο XML-RPC εξυπηρετητής ανέφερε: %s."

#: ../hig_alert.py:17
#, python-format
msgid "Unknown username %s or password trying to post blog entry to XML-RPC server %s."
msgstr "Άγνωστο όνομα χρήστη %s ή κωδικός πρόσβασης κατά την προσπάθεια δημοσίευσης καταχώρησης ιστολογίου στον εξυπηρετητή XML-RPC %s."

#: ../hig_alert.py:19
#, python-format
msgid "Could not post to blog '%s' at bloggerAPI XML-RPC server '%s'. Server reported: %s."
msgstr "Δεν ήταν δυνατή η δημοσίευση στο ιστολόγιο '%s' στον bloggerAPI εξυπηρετητή XML-RPC '%s'. O εξυπηρετητής ανέφερε: %s."

#: ../hig_alert.py:21
#, python-format
msgid ""
"The bloggerAPI server (%s) reported an error I don't understand: '%s, %s'. \n"
"\n"
"Please email this error message to seth@gnome.org so I can address it."
msgstr ""
"Ο εξυπηρετητής bloggerAPI (%s) ανέφερε ένα σφάλμα που είναι ακατανόητο: '%s, %s'. \n"
"\n"
"Παρακαλώ στείλτε με μήνυμα ηλεκτρονικού ταχυδρομείου αυτό το μήνυμα σφάλματος στο seth@gnome.org έτσι ώστε να διευθετηθεί."

#: ../protocols/advogato.py:27
#: ../protocols/advogato.py:30
#: ../protocols/advogato.py:42
#: ../protocols/bloggerAPI.py:61
#: ../protocols/bloggerAPI.py:89
#: ../protocols/livejournal.py:35
#: ../protocols/livejournal.py:64
msgid "Could not post Blog entry"
msgstr "Δεν ήταν δυνατή η δημοσίευση της καταχώρησης ιστολογίου"

#: ../protocols/advogato.py:27
msgid "Your username is invalid. Please double-check the preferences."
msgstr "Το όνομα χρήστη σας είναι μη έγκυρο. Παρακαλώ ελέγξτε ξανά τις προτιμήσεις σας."

#: ../protocols/advogato.py:30
#: ../protocols/livejournal.py:35
msgid "Your username or password is invalid. Please double-check the preferences."
msgstr "Το όνομα χρήστη ή ο κωδικός πρόσβασης είναι μη έγκυρος. Παρακαλώ ελέγξτε ξανά τις προτιμήσεις σας."

#: ../protocols/advogato.py:39
#: ../protocols/bloggerAPI.py:86
#: ../protocols/livejournal.py:61
msgid "Could not post blog entry"
msgstr "Δεν ήταν δυνατή η δημοσίευση της καταχώρησης ιστολογίου"

#: ../protocols/advogato.py:42
#: ../protocols/bloggerAPI.py:38
#: ../protocols/bloggerAPI.py:89
#, python-format
msgid "URL '%s' does not seem to be a valid bloggerAPI XML-RPC server. Web server reported: %s."
msgstr "Το URL '%s' δε φαίνεται να είναι ένας έγκυρος bloggerAPI εξυπηρετητής XML-RPC. Ο εξυπηρετητής ιστού ανέφερε: %s."

#. TODO: consider GNOME proxy configurations, as in bloggerAPI.py
#: ../protocols/atomBloggerAPI.py:24
#| msgid "Could not get list of blogs"
msgid "Could not get list of blogs."
msgstr "Αδυναμία λήψης καταλόγου των ιστολογίων."

#: ../protocols/atomBloggerAPI.py:29
#: ../protocols/atomBloggerAPI.py:99
msgid "A captcha was required for authentication."
msgstr "Απαιτείται captcha για πιστοποίηση"

#: ../protocols/atomBloggerAPI.py:32
#: ../protocols/atomBloggerAPI.py:102
msgid "Username or password was invalid."
msgstr "Το όνομα χρήστη ή ο κωδικός πρόσβασης δεν ήταν έγκυρα"

#: ../protocols/atomBloggerAPI.py:35
#: ../protocols/atomBloggerAPI.py:105
#, python-format
msgid "Failed to login: %s"
msgstr "Αποτυχία σύνδεσης: %s"

#: ../protocols/atomBloggerAPI.py:38
#: ../protocols/atomBloggerAPI.py:108
#, python-format
msgid "Socket error: %s.  Perhaps try again."
msgstr "Σφάλμα υποδοχής: %s. Ίσως να προσπαθήσετε ξανά."

#. No blogs found
#: ../protocols/atomBloggerAPI.py:45
msgid "No Blogs Found"
msgstr "Δε βρέθηκαν ιστολόγια"

#: ../protocols/atomBloggerAPI.py:45
#, python-format
msgid "No errors were reported, but no blogs were found at %s for %s\n"
msgstr "Δεν αναφέρθηκαν λάθη, αλλά δεν βρέθηκαν ιστολόγια στο %s για %s\n"

#: ../protocols/atomBloggerAPI.py:95
#| msgid "Could not post Blog entry"
msgid "Could not post entry."
msgstr "Δεν ήταν δυνατή η δημοσίευση της καταχώρησης"

#: ../protocols/atomBloggerAPI.py:115
#, python-format
msgid "Failed to post: %s"
msgstr "Αποτυχία δημοσίευσης: %s"

#: ../protocols/bloggerAPI.py:35
#: ../protocols/bloggerAPI.py:38
msgid "Could not get list of blogs"
msgstr "Δεν ήταν δυνατή η λήψη της λίστας των ιστολογίων"

#: ../protocols/bloggerAPI.py:61
msgid "No XML-RPC server URL to post blog entries to is set, or the value could not be retrieved from GConf. Your entry will remain in the blogger window."
msgstr "Δεν έχει οριστεί το URL κάποιου XML-RPC εξυπηρετητή για δημοσίευση καταχωρήσεων ιστολογίου, ή η τιμή δεν μπορεί να ανακτηθεί από το GConf. Η καταχώρησή σας θα παραμείνει στο παράθυρο ιστολογίου."

#: ../protocols/livejournal.py:64
#, python-format
msgid "URL '%s' does not seem to be a valid LiveJournal XML-RPC server. Web server reported: %s."
msgstr "Το URL '%s' δε φαίνεται να είναι ένας έγκυρος LiveJournal εξυπηρετητής XML-RPC. Ο εξυπηρετητής ιστού ανέφερε: %s."

#: ../rich_entry.py:144
#: ../rich_entry.py:152
msgid "Could not load dragged in image"
msgstr "Δεν ήταν δυνατή η φόρτωση των εικόνων που σύρθηκαν μέσα"

#: ../rich_entry.py:145
#: ../rich_entry.py:153
#, python-format
msgid "Error loading %s was: %s"
msgstr "Το σφάλμα φόρτωσης %s ήταν: %s"

#: ../rich_entry.py:210
msgid "Add _Link..."
msgstr "Προσθήκη _συνδέσμου..."

#: ../rich_entry.py:215
msgid "Add Link"
msgstr "Προσθήκη συνδέσμου"

#: ../rich_entry.py:218
msgid "_Add Link"
msgstr "Προσθήκη _συνδέσμου"

#: ../rich_entry.py:226
msgid "Text:"
msgstr "Κείμενο:"

#: ../rich_entry.py:229
msgid "URL:"
msgstr "URL:"

